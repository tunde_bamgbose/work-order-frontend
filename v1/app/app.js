var app = angular.module('app',[]);

const API_URL = 'https://www.hatchways.io/api/assessment';

app.controller('homeCtrl',function($scope,$http){

    $scope.work_orders = [];
    $scope.temp = [];
    $scope.orderParam = 'deadline';

    $scope.checkbox = {
        selected: false
    };

    $http.get(`${API_URL}/work_orders`).then(function(res){
        $scope.work_orders = res.data.orders ? res.data.orders : [];

        $scope.work_orders.forEach(element => {
            $http.get(`${API_URL}/workers/${ element.workerId }`).then(function(user){
                element['worker'] = user.data.worker;
            });
        });
    },function(error){
        console.log(error);
    });

    $scope.toggle = function(){

            if($scope.checkbox.selected){
                $scope.orderParam = '-deadline';
            }
            else{
                $scope.orderParam = 'deadline';
            }
    

    }
    

})