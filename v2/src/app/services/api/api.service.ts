import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, share, shareReplay, refCount, take, publishReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

const API_URL = "https://www.hatchways.io/api/assessment";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  cache: Observable<any>;

  constructor(private http: HttpClient) { }

  getOrders(){
    return this.http.get(`${ API_URL }/work_orders`).pipe(map(data => data));
  }

  getWorker(worker_id){
    return this.http.get(`${ API_URL }/workers/${ worker_id }`)
      .pipe(
        map((data:any)=> data.worker),
        share(),
        publishReplay(1),
        refCount(),
        take(1)
      );
  }
}