import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';
import { Observable, of } from 'rxjs';
import { map, share } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  work_orders: any = [];
  filtered_orders: any = [];
  checkbox = {
    selected: false
  };
  search: string;
  order: boolean = false;
  orderType : string = "deadline";

  private observableCache: { [key: number]: Observable<any> } = {};
  private workerCache: { [key: number]: any } = {};

  constructor(private api:ApiService) {
  }

  ngOnInit() {
    this.loadData();
  }

  async loadData(){
    try{
      let orders = await this.api.getOrders();

      orders.subscribe(async (res: any)=>{
        this.work_orders = res.orders;
  
        // get each worker
        this.work_orders.forEach(async(order) => {
          let worker = await this.getWorkerById(order.workerId);

            worker.subscribe(user=>{
              order["worker"] = user;
            });
        });


        this.filtered_orders = this.work_orders;
      },(err)=>{
        console.log(err.message);
      })
    }
    catch(err){
      console.log(err);
    }
  }

  searchOrdersByWorkerName(){
      this.filtered_orders = this.work_orders;
      this.filtered_orders = this.filtered_orders.filter(orders => orders.worker.name.toLowerCase().indexOf(this.search.toLowerCase()) !== -1 );
  }

  toggle(){
    if(this.checkbox.selected){
      this.order = true;
    }
    else{
      this.order = false;
    }
  }

  getWorkerById(id){
    // Data available
    if(this.workerCache[id]) return of(this.workerCache[id]);
    // Request pending
    else if (this.observableCache[id]) return this.observableCache[id];
    // New request needed
    else this.observableCache[id] = this.fetchWorker(id);

    return this.observableCache[id];
  }

  private fetchWorker(id){
    return this.api.getWorker(id).pipe(
      map(data => this.matchData(data)),
      share()
    );
  }

  private matchData(data){
    this.observableCache[data.workerId] = null;
    this.workerCache[data.workerId] = data;
    return this.workerCache[data.workerId];
  }
}
