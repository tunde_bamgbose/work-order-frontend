# Work Orders

## How to run

Copy everything within the dist/ folder to a folder named hatchways on a web server.

## Development server

Make sure node js and angular cli is installed.

Cd in directory and run `npm install` to install dependencies

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 
